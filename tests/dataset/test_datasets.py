from src.dataset.datasets import Datasets


def test_datasets_init():
    datasets = Datasets(train="train", validation="validation")
    assert type(datasets) == Datasets
    assert datasets.train == "train"
    assert datasets.validation == "validation"
