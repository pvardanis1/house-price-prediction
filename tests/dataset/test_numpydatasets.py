import numpy as np
import pydantic
import pytest

from src.dataset.datasets import Datasets
from src.dataset.numpydatasets import NumpyDatasets


@pytest.fixture
def train_dataset():
    return [np.random.random((10, 10)), np.random.random((1, 10))]


def test_numpy_datasets_init(train_dataset):
    datasets = NumpyDatasets(train=train_dataset)
    assert isinstance(datasets, Datasets)
    assert type(datasets) == NumpyDatasets
    np.testing.assert_array_equal(datasets.train[0], train_dataset[0])
    np.testing.assert_array_equal(datasets.train[1], train_dataset[1])


def test_numpy_datasets_init_raise_error_if_invalid_dataset(train_dataset):
    train_dataset = "invalid-dataset"
    with pytest.raises(pydantic.ValidationError):
        NumpyDatasets(train=train_dataset)
