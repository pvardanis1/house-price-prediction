SOURCE=src

.PHONY: build_docs
build_docs:
	cd docs && $(MAKE) html

.PHONY: checklist
checklist: reformat interrogate lint typehint test 

.PHONY: clean
clean:
	find . -type f -name "*.pyc" | xargs rm -fr
	find . -type d -name __pycache__ | xargs rm -fr

.PHONY: install
install:
	poetry install --no-dev

.PHONY: install_dev
install_dev:
	poetry install

.PHONY: install_poetry
install_poetry:
	pip install poetry

.PHONY: interrogate
interrogate:
	poetry run interrogate ${SOURCE} -c pyproject.toml --generate-badge interrogate.svg -vv

.PHONY: lint
lint:
	poetry run pylint ${SOURCE}

.PHONY: pre-commit
pre-commit:
	poetry run pre-commit run

.PHONY: reformat
reformat:
	poetry run isort ${SOURCE} tests
	poetry run black ${SOURCE} tests

.PHONY: reformatdiff
reformatdiff:
	poetry run black --diff ${SOURCE}

.PHONY: test
test:
	poetry run pytest --cov-report term-missing --cov-report xml:coverage.xml --cov=${SOURCE}

.PHONY: typehint
typehint:
	poetry run mypy ${SOURCE}