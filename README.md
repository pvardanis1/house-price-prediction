# House Price Prediction

## Getting started

On this project, we created a framework for training and explaining tree-based ML models. The framework is tool-agnostic and provides interfaces that can be extended to potentially support any ML-library of your choice.

The implementation follows a near production-ready setup focusing more in modular and maintainable code rather than experiment results.


As a basis, we will train a Random Forest Regressor model to predict the house prices in King County. Next, we will try to interpret model's decision to reach the final prediction.

## Install

The current setup has been tested on a MacOS M1 machine.

First, you need to install `Python 3.8` in a virtual environment of your choice, either by using [venv](https://docs.python.org/3/library/venv.html) or [anaconda](https://www.anaconda.com/) (or other solutions you may be more familiar with).

For dependency management, we use [poetry](https://python-poetry.org/).

Assuming you're inside the virtual environment, you can install [poetry](https://python-poetry.org/) with the following `make` command:
```bash
make install_poetry
```

Finally you can install project dependencies as follows:
```bash
make install_dev
```

# Tools

We use several tools that contribute to a more production-ready environment, namely:
- [`interrogate`](https://interrogate.readthedocs.io/en/latest/) for testing documentation coverage;
- [`pylint`](https://pylint.org/) for static code analysis;
- [`mypy`](http://mypy-lang.org/) for static type checking;
- [`pytest`](https://docs.pytest.org/en/7.2.x/) for running tests.

These tools run automatically in `gitlab-ci.yml` by using the following commands respectively:
- `make interrogate`
- `make lint`
- `make typehint`
- `make test`

You can also run the above commands one-by-one locally. Alternatively, you can run them all together with the following command:
```bash
make checklist
```

# Notebooks

In the [`notebooks`](https://gitlab.com/pvardanis1/house-price-prediction/-/tree/main/notebooks) folder, we provide two notebooks:
- `train.ipynb` for training a Random Forest Regressor on the house prediction task;
- `explain.ipynb` for explaining the trained model from the previous step using [SHAP (SHapley Additive exPlanations)](https://en.wikipedia.org/wiki/Shapley_value).

# Acknowledgments

Given the time limitation this project is by no means 100% ready to be used in a production-ready environment.

Here are some potential improvements:
- Add logging to provide verbose information while code is running;
- Add 100% test coverage. We only did two tests to showcase the potential, but didn't have the time to create tests for everything;
- Data loading is not optimized for big datasets. We also could add sharding or `tfrecord` functionalities to increase performance;
- Add an Evaluator interface that helps evaluating our models with a given set of metrics on test data;
- `ShapExplainer` works only with `SupportedTreeModels`, we can expand this `Enum` as soon as more algorithms are implemented;
- Experiment with other explainability methods like Recursive Feature Elimination, Impurity-based Feature Importance (e.g., in Random Forest), Partial Dependency Plot etc.
- Create abstractions and implementations for other functionalities that were used within the notebooks (e.g., visualization, tree graph export to png files etc.)