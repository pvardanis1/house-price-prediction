"""This module features the ShapExplainer class."""

from enum import Enum
from typing import Any, Callable, Optional

import pandas as pd
import shap

from src.explainability.modelexplainer import ModelExplainer
from src.models.scikitmodel import ScikitModel


class SupportedTreeModels(Enum):
    """This enum class contains all tree-based models
    in our frameworks supported by SHAP."""

    SCIKIT_RF_REGRESSOR = "ScikitRFRegressor"


class ShapExplainer(ModelExplainer):
    """This class applies the SHAP explaining strategy to a given model.

    SHAP is a game theoretic approach to explain the output of tree models.
    Currently the following implementations are supported: XGBoost, LightGBM,
    CatBoost, scikit-learn and pyspark. For the ease of simplicity, we will
    assume that it only works with scikit-learn models that implement the
    ScikitModel interface. Ideally all tree based models implemented in
    our framework should be part of a TreeModel interface,
    and ScikitModel should inherit from it.
    """

    def __init__(self, model: ScikitModel, dataset: pd.DataFrame) -> None:
        self._raise_type_error_if_model_not_supported(model)
        self.model = model
        self.dataset = dataset
        self._results: Optional[
            shap._explanation.Explanation
        ] = None  # can be used for additional visualizations
        self._explainer: Optional[shap.Explainer] = None

    @property
    def explainer(self) -> shap.Explainer:
        """This property returns the explainer object.

        :return: The explainer object.
        """
        if self._explainer is None:
            raise ValueError(
                "The explainer object is not initialized. "
                "Please call the explain method first."
            )
        return self._explainer

    @property
    def explainer_cls(self) -> Callable:
        """This property returns the explainer object."""
        return shap.Explainer

    @property
    def results(self) -> shap._explanation.Explanation:
        """This property returns the results of the explain method."""
        if self._results is None:
            raise ValueError(
                "The explain method must be called before "
                "accessing the results property."
            )
        return self._results

    def explain(self) -> pd.Series:
        """This method explains the model and returns
        explainable values per feature stored in a pd.Series object.

        :return: A pandas Series object with explainability values per feature
        in a tree-based model.
        """
        self._explainer = self.explainer_cls(self.model.model)  # type: ignore
        self._results = self._explainer(self.dataset)
        average_shap_values_per_feature = (
            self._get_average_shap_values_per_feature()
        )
        return average_shap_values_per_feature

    def _get_average_shap_values_per_feature(self) -> pd.Series:
        """This method creates a pandas Series object with average
        shap values per feature in a tree-based model.

        :return: A pandas Series object with explainability values per feature
        in a tree-based model.
        """
        shap_values_per_sample = pd.DataFrame(
            self.results.values,
            columns=self.results.feature_names,
            index=self.dataset.index,
        )
        average_shap_values_per_feature = shap_values_per_sample.T.mean(axis=1)
        return average_shap_values_per_feature

    def _raise_type_error_if_model_not_supported(self, model: Any) -> None:
        model_type = model.__class__.__name__
        if model_type not in [item.value for item in SupportedTreeModels]:
            raise TypeError(
                f"Model {model_type} is not supported by "
                f"{self.__class__.__name__}."
            )
