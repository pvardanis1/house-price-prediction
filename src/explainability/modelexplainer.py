"""This module features the ModelExplainer interface."""


from abc import ABC, abstractmethod
from typing import Callable

import pandas as pd


class ModelExplainer(ABC):
    """This class serves as an interface for all model explainers.

    Given a trained MLModel, this class will explain the model using
    either the very same model or another model. For example, a
    RandomForestRegressor can be explained using the feature importance
    property or a LinearRegression model can be explained via the
    Recursive Feature Elimination method. It is up to the subclass
    implementation to decide which method to use.
    """

    @property
    @abstractmethod
    def explainer_cls(self) -> Callable:
        """This property returns the explainer object."""

    @abstractmethod
    def explain(self) -> pd.Series:
        """This method explains the model and returns
        explainable values per feature stored in a pd.Series object.

        :return: A pandas Series object with explainability values per feature
        in a tree-based model.
        """
