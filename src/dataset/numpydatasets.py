"""This module features the NumpyDatasets dataclass."""

from typing import List, Optional

import numpy as np
from pydantic import Extra

from src.dataset.datasets import Datasets


class NumpyDatasets(Datasets):
    """This class owns a train and a validation dataset for training a ScikitModel.

    Attributes:
        - train: A list of two elements training data containing the
        features and labels respectively.
        - validation: A list of two elements training data containing
        the features and labels respectively.
    """

    train: List[np.ndarray]
    validation: Optional[List[np.ndarray]] = None

    class Config:
        """Config class for Pydantic"""

        arbitrary_types_allowed = True
        extra = Extra.forbid
