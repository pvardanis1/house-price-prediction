"""This module features the Datasets base class."""

from typing import Any, Optional

from pydantic import BaseModel, Extra


class Datasets(BaseModel):
    """This class is a base class for storing datasets."""

    train: Any
    validation: Optional[Any] = None

    class Config:
        """Config class for Pydantic"""

        arbitrary_types_allowed = True
        extra = Extra.forbid
