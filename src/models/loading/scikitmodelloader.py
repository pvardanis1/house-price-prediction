"""This module features the ScikitRFRegressor object loaders."""

from pathlib import Path

from src.io.loading.pickleloader import PickleLoader
from src.models.scikitrfregressor import ScikitRFRegressor


def load_scikit_rf_regressor_model(path: Path) -> ScikitRFRegressor:
    """Load a ScikitRFRegressor model from a pickle file.

    :param path: Path to the pickle file.
    :return: A ScikitRFRegressor instance.
    """
    raw_model = PickleLoader().load(path)
    return ScikitRFRegressor(model=raw_model)
