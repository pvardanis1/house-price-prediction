"""This module features the MLModel abstract class."""

from typing import Any, Optional

from pydantic import BaseModel, Extra


class MLModel(BaseModel):
    """MLModel provides a base class for machine learning models.

    Attributes:
        - name: Name of the model
        - model: Raw model (e.g., a Keras or scikit-learn model). Specific type
            should be specified by subclasses
    """

    name: Optional[str]
    model: Optional[Any]

    class Config:
        """Config class for Pydantic"""

        arbitrary_types_allowed = True
        extra = Extra.forbid
