"""This module features the Scikit abstract class."""

from abc import abstractmethod
from typing import Callable, Optional

from pydantic import Extra
from sklearn.base import BaseEstimator

from src.models.mlmodel import MLModel


class ScikitModel(MLModel):
    """ScikitModel provides a wrapper for the scikit-learn BaseEstimator model.

    Attributes:
        - name: Name of the model
        - model: Raw model (e.g., a scikit-learn model). All sci-kit learn models
        are subclasses of BaseEstimator, mixin classes are used in addition to add
        different types of functionality (i.e., transformers, classifiers, regressors etc.)
    """

    name: Optional[str] = ""
    model: Optional[BaseEstimator] = None

    @property
    @abstractmethod
    def model_cls(self) -> Callable:
        """Return the scikit model class."""

    class Config:
        """Config class for Pydantic"""

        arbitrary_types_allowed = True
        extra = Extra.forbid
