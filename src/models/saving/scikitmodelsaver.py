"""This module features the ScikitRFRegressor object savers."""

from pathlib import Path

from src.io.writing.picklewriter import PickleWriter
from src.models.scikitmodel import ScikitModel


def save_scikit_model(model: ScikitModel, path: Path) -> None:
    """Save a ScikitModel instance to a pickle file.

    :param model: A ScikitModel instance.
    """
    raw_model = model.model
    PickleWriter().write(raw_model, path)
