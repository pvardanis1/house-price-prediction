"""This module features the ScikitRFRegressor class."""

from typing import Callable

from pydantic import Extra
from sklearn.ensemble import RandomForestRegressor

from src.models.scikitmodel import ScikitModel


class ScikitRFRegressor(ScikitModel):
    """ScikitModel provides a wrapper for the scikit-learn RandomForestRegressor model.

    Attributes:
        - name: Name of the model
        - model: Raw model (e.g., a scikit-learn model). All scikit-learn models
        are subclasses of BaseEstimator, mixin classes are used in addition to add
        different types of functionality (i.e., transformers, classifiers, regressors etc.)
    """

    @property
    def model_cls(self) -> Callable:
        return RandomForestRegressor

    class Config:
        """Config class for Pydantic"""

        arbitrary_types_allowed = True
        extra = Extra.forbid
