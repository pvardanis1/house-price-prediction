"""This module features the ScikitRFRegressor class."""

from enum import IntEnum

from pydantic import BaseModel, StrictInt, StrictStr

from src.models.scikitrfregressor import ScikitRFRegressor


class VerboseEnum(IntEnum):
    """Enum for verbosity levels."""

    VALUE_1 = 0
    VALUE_2 = 1
    VALUE_3 = 2
    VALUE_4 = 3


class ScikitRFRegressorParams(BaseModel):
    """ScikitRFRegressorParams provides parameters for the ScikitRFRegressor model."""

    name: StrictStr
    n_estimators: StrictInt
    max_features: StrictStr
    max_depth: StrictInt
    random_state: StrictInt
    verbose: VerboseEnum = VerboseEnum.VALUE_1


def create_scikit_rf_regressor(
    parameters: ScikitRFRegressorParams,
) -> ScikitRFRegressor:
    """Wrapper around Keras model compile.

    :param parameters: Parameters for the ScikitRFRegressor instance.
    :return: A ScikitRFRegressor instance.
    """
    rf_regressor = ScikitRFRegressor(name=parameters.name)
    model_params = {k: v for k, v in parameters.dict().items() if k != "name"}
    rf_regressor.model = rf_regressor.model_cls(**model_params)
    return rf_regressor
