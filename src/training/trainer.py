"""This module features the Trainer abstract class."""

from abc import ABC, abstractmethod
from typing import Any, Optional

import pydantic

from src.dataset.datasets import Datasets
from src.models.mlmodel import MLModel


class Trainer(ABC):
    """This class provides an interface for training MLModel objects."""

    expected_model_type: pydantic.main.ModelMetaclass = MLModel
    expected_datasets_type: pydantic.main.ModelMetaclass = Datasets

    def __init__(self) -> None:
        self._model: Optional[MLModel] = None
        self._datasets: Optional[Datasets] = None

    @property
    def model(self) -> MLModel:
        """The model to train."""
        return self._model  # type: ignore

    @property
    def datasets(self) -> Datasets:
        """The datasets to train."""
        return self._datasets  # type: ignore

    @abstractmethod
    def train(self) -> None:
        """Train an MLModel on a Datasets object."""

    def _raise_error_if_model_is_not_assigned(self) -> None:
        if not isinstance(self._model, self.expected_model_type):
            message = "Model has not been assigned."
            raise TypeError(message)

    def _raise_error_if_datasets_is_not_assigned(self) -> None:
        if not isinstance(self._datasets, self.expected_datasets_type):
            message = "Datasets has not been assigned."
            raise TypeError(message)

    def _raise_type_error_if_model_is_wrong_type(self, model: Any) -> None:
        if not isinstance(model, self.expected_model_type):
            object_name = self.expected_model_type.__name__
            message = f"Model must be a {object_name}. Got type {type(model)} instead."
            raise TypeError(message)

    def _raise_type_error_if_datasets_is_wrong_type(
        self, datasets: Any
    ) -> None:
        if not isinstance(datasets, self.expected_datasets_type):
            object_name = self.expected_datasets_type.__name__
            message = f"Datasets must be a {object_name}. Got type {type(datasets)} instead."
            raise TypeError(message)
