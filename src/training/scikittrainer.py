"""This module features the ScikitTrainer class."""


import pydantic

from src.dataset.numpydatasets import NumpyDatasets
from src.models.scikitmodel import ScikitModel
from src.training.trainer import Trainer


class ScikitTrainer(Trainer):
    """This class is used for training a KerasModel."""

    expected_model_type: pydantic.main.ModelMetaclass = ScikitModel
    expected_datasets_type: pydantic.main.ModelMetaclass = NumpyDatasets

    @Trainer.model.setter  # type: ignore
    def model(self, model: ScikitModel) -> None:
        """Set the model to train."""
        self._raise_type_error_if_model_is_wrong_type(model)
        self._model = model

    @Trainer.datasets.setter  # type: ignore
    def datasets(self, datasets: NumpyDatasets) -> None:
        """Set the model to train."""
        self._raise_type_error_if_datasets_is_wrong_type(datasets)
        self._datasets = datasets

    def train(self) -> None:
        """Train a ScikitModel on a NumpyDatasets object."""

        self._raise_error_if_model_is_not_assigned()
        self._raise_error_if_datasets_is_not_assigned()

        self._model.model.fit(*self._datasets.train)  # type: ignore


def create_scikit_trainer(
    model: ScikitModel, datasets: NumpyDatasets
) -> ScikitTrainer:
    """Wrapper around ScikitTrainer constructor.

    :param model: The model to train.
    :param datasets: The datasets to train on.
    :return: A ScikitTrainer instance.
    """
    scikit_trainer = ScikitTrainer()
    scikit_trainer.model = model
    scikit_trainer.datasets = datasets
    return scikit_trainer
