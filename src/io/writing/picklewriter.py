"""This module features the PickleWriter class."""

import pickle
from pathlib import Path
from typing import Any

from src.io.writing.writer import Writer


class PickleWriter(Writer):
    """PickleWriter writes data to a local pickle file."""

    allowed_extensions = ["pickle", "pkl"]

    def write(self, data: Any, filepath: Path) -> None:
        """Write data to a pickle file.
        :param filepath: (str) path where to write data to
        """
        self._raise_error_if_cannot_ingest(filepath)
        with open(filepath.as_posix(), "wb") as file:
            pickle.dump(data, file)
