"""This module contains the implementation of the Loader interface."""

from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, List


class Loader(ABC):
    """Loader is the interface that defines how a file should be loaded."""

    allowed_extensions: List[str] = []

    @staticmethod
    def _get_extension(filepath: Path) -> str:
        """Get the extension of a file."""
        return filepath.suffix[1:]

    @classmethod
    def _can_ingest(cls, filepath: Path) -> bool:
        """Checks if the file extension is allowed."""
        suffix = cls._get_extension(filepath)
        return suffix in cls.allowed_extensions

    @classmethod
    def _raise_error_if_cannot_ingest(cls, filepath: Path) -> None:
        if not cls._can_ingest(filepath):
            raise ValueError(
                f"File format '{cls._get_extension(filepath)}' is not allowed"
            )

    @abstractmethod
    def load(self, filepath: Path) -> Any:
        """Method to load the data."""
