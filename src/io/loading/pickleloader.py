"""This module features the PickleLoader class."""

import pickle
from pathlib import Path
from typing import Any

from src.io.loading.loader import Loader


class PickleLoader(Loader):
    """PickleLoader loads data from a local pickle file."""

    allowed_extensions = ["pickle", "pkl"]

    def load(self, filepath: Path) -> Any:
        """Load data from a pickle file.

        :param filepath: (str) path where to load data from
        """
        self._raise_error_if_cannot_ingest(filepath)
        with open(filepath.as_posix(), "rb") as file:
            data = pickle.load(file)

        return data
