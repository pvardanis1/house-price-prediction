"""This module features the CSVLoader class."""

from pathlib import Path

import pandas as pd

from src.io.loading.loader import Loader


class CSVLoader(Loader):
    """CSVLoader loads data from a local CSV file."""

    allowed_extensions = ["csv"]

    def load(self, filepath: Path) -> pd.DataFrame:
        """Load data to a pd.DataFrame.

        :param filepath: (str) path where to load data from
        """
        self._raise_error_if_cannot_ingest(filepath)
        data = pd.read_csv(filepath)
        return data
